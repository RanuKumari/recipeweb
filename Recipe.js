let ingredients = [];

function addIngred()
{
    var name = document.getElementById('name').value;
    var ingredient = document.getElementById('ingredient').value;
    
    // console.log(name);
    // console.log(ingredients);

    if(name&&ingredient)
    {
        let ingredObj = {name: name, ingredient: ingredient};
        ingredients.push(ingredObj);
        alert('Ingredient Successfully Added');
        changeTable();
    }
    //console.log(ingredients);

}

function deleteIngred(index)
    {
        if(index<ingredients.length)
        {
            let confirmDel = confirm('Are you sure you want to delete?');
            if(confirmDel)
            {
                ingredients.splice(index,1);
                alert('Ingredient Successfully Deleted');
                changeTable();
            }
        }
    }

function changeTable()
{
    var tableBody = document.querySelector('#tablebody');
    //console.log(tableBody);
    tablebody.innerHTML = '';
    console.log(tableBody);
    for(let i=0;i<ingredients.length;i++)
    {
        var row = 
                `<tr>
                    <td>${ingredients[i].name}</td>
                    <td>${ingredients[i].ingredient}</td>
                    <td>
                        <button onclick="deleteIngred(${i})"><i class="bi bi-trash"></i></button>
                    </td>
                </tr>`;
                tableBody.innerHTML +=row;
    }
    
    console.log(tableBody);
}

document.getElementById("myForm").addEventListener("submit",function(event){
    event.preventDefault()
})
